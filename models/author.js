const mongoose = require('mongoose');
const { DateTime } = require('luxon');

const Schema = mongoose.Schema;

const AuthorSchema = new Schema({
    first_name: {
        type: String,
        required: true,
        maxLength: 100
    },
    family_name: {
        type: String,
        required: true,
        maxLength: 100
    },
    date_of_birth: { type: Date },
    date_of_death: { type: Date }
}, {
    virtuals: {
        fullName: {
            get() {
                if (this.first_name && this.family_name) {
                    return `${this.first_name} ${this.family_name}`;
                }
            }
        },
        url: {
            get() {
                return `/catalog/author/${this._id}`;
            }
        },
        dates: {
            get() {
                let date_of_death_formatted = '';
                let date_of_birth_formatted = DateTime.fromJSDate(this.date_of_birth).toLocaleString(DateTime.DATE_MED);
                if (this.date_of_death) {
                    date_of_death_formatted = DateTime.fromJSDate(this.date_of_death).toLocaleString(DateTime.DATE_MED);
                }
                return `${date_of_birth_formatted} - ${date_of_death_formatted}`;
            }
        }
    }

});

// // Virtual for author's full name
// AuthorSchema.virtual('name').get(function () {
//     let fullName = '';
//     if (this.first_name && this.last_name) {
//         fullName = `${this.first_name}, ${this.last_name}`;
//     }
//     return fullName;
// });
//
// // Virtual for author's URL
// AuthorSchema.virtual('url').get(function () {
//     return `/catalog/author/${this._id}`;
// });

// Export model
module.exports = mongoose.model('Author', AuthorSchema);
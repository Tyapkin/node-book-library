const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const GenreSchema = new Schema({
    name: { type: String, require: true, maxLength: 100, minLength: 3 },
}, {
    virtuals: {
        url: {
            get() {
                return `/catalog/genre/${this._id}`;
            }
        }
    }
});

module.exports = mongoose.model('Genre', GenreSchema);
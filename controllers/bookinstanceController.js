const BookInstance = require("../models/bookinstance");
const Book = require('../models/book');
const async = require('async');

// Display list of all BookInstances.
exports.bookinstance_list = (req, res, next) => {
    BookInstance.find()
        .populate('book')
        .exec(
            function (err, list_bookinstances) {
                if (err) {
                    return next(err)
                }
                res.render('bookinstance_list', {
                    title: 'Book instance list',
                    bookinstance_list: list_bookinstances
                });
            }
        );
};

// Display detail page for a specific BookInstance.
exports.bookinstance_detail = (req, res, next) => {
    BookInstance.findById(req.params.id)
        .populate('book')
        .exec(function (err, result) {
            if (err) {
                return next(err);
            }
            res.render('bookinstance_detail', {
                title: 'Book instance detail',
                book_instance: result
            });
        });
};

// Display BookInstance create form on GET.
exports.bookinstance_create_get = (req, res) => {
    res.send("NOT IMPLEMENTED: BookInstance create GET");
};

// Handle BookInstance create on POST.
exports.bookinstance_create_post = (req, res) => {
    res.send("NOT IMPLEMENTED: BookInstance create POST");
};

// Display BookInstance delete form on GET.
exports.bookinstance_delete_get = (req, res) => {
    res.send("NOT IMPLEMENTED: BookInstance delete GET");
};

// Handle BookInstance delete on POST.
exports.bookinstance_delete_post = (req, res) => {
    res.send("NOT IMPLEMENTED: BookInstance delete POST");
};

// Display BookInstance update form on GET.
exports.bookinstance_update_get = (req, res) => {
    res.send("NOT IMPLEMENTED: BookInstance update GET");
};

// Handle bookinstance update on POST.
exports.bookinstance_update_post = (req, res) => {
    res.send("NOT IMPLEMENTED: BookInstance update POST");
};